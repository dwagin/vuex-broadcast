import { BroadcastChannel } from 'broadcast-channel'

export default class VuexBroadcastPlugin {
  #channel
  #mutationsNames = []
  #incoming = false

  constructor ({ channelName = 'vuexBroadcast', channelOptions = {} } = {}) {
    this.#channel = new BroadcastChannel(
      channelName,
      { ...{ webWorkerSupport: false }, ...channelOptions }
    )
    return store => {
      this.#init(store)
    }
  }

  #init (store) {
    const root = store._modules.root._rawModule
    if (root.broadcast === true) {
      this.#mutationsNames = this.#mutationsNames.concat(Object.keys(root.mutations))
    }

    const modules = store._modules.root._rawModule.modules
    for (const moduleName in modules) {
      const moduleConfig = modules[moduleName]
      if (moduleConfig.broadcast === true) {
        this.#mutationsNames = this.#mutationsNames.concat(
          Object.keys(moduleConfig.mutations).map(mutation => `${moduleName}/${mutation}`)
        )
      }
    }

    this.#channel.addEventListener('message', message => {
      this.#onMessage(message, store)
    })

    store.subscribe(mutation => {
      if (!this.#incoming) {
        this.#postMutation(mutation)
      }
    })
  }

  #onMessage (mutation, store) {
    if (this.#mutationsNames.includes(mutation?.type)) {
      try {
        this.#incoming = true
        store.commit(mutation.type, mutation.payload)
      } finally {
        this.#incoming = false
      }
    }
  }

  #postMutation (mutation) {
    if (this.#mutationsNames.includes(mutation?.type)) {
      this.#channel.postMessage(mutation)
    }
  }
}
